use crate::audio::initialise_audio;
use crate::config::PongConfig;
use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    core::math::Vector2,
    core::{timing::Time, transform::Transform},
    ecs::prelude::{Component, DenseVecStorage, Entity, NullStorage},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    ui::{Anchor, TtfFormat, UiText, UiTransform},
};

#[derive(Default)]
pub struct Pong {
    ball_spawn_timer: Option<f32>,
    sprite_sheet_handle: Option<Handle<SpriteSheet>>,
    initialised: bool,
}

impl SimpleState for Pong {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        self.ball_spawn_timer.replace(5.0);
        self.initialised = false;

        world.register::<Ball>();
        self.sprite_sheet_handle
            .replace(load_sprite_spreadsheet(world));

        initialise_paddles(world, self.sprite_sheet_handle.clone().unwrap());
        initialise_camera(world);
        initialise_scoreboard(world);
        initialise_audio(world);
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if !self.initialised {
            if let Some(mut timer) = self.ball_spawn_timer.take() {
                {
                    let time = data.world.fetch::<Time>();
                    timer -= time.delta_seconds();
                }
                if timer <= 0.0 {
                    self.initialised = true;
                    initialise_ball(
                        data.world,
                        self.sprite_sheet_handle
                            .clone()
                            .expect("can't unwrap spritesheet of ball"),
                    );
                } else {
                    self.ball_spawn_timer.replace(timer);
                }
            }
        }
        Trans::None
    }
}

pub struct Ball {
    pub velocity: [f32; 2],
    pub radius: f32,
}

impl Component for Ball {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Default)]
pub struct ScoreBoard {
    pub score_left: i32,
    pub score_right: i32,
}

pub struct ScoreText {
    pub p1_score: Entity,
    pub p2_score: Entity,
}

#[derive(PartialEq, Eq)]
pub enum Side {
    Left,
    Right,
}

pub struct Paddle {
    pub side: Side,
    pub width: f32,
    pub height: f32,
}

#[derive(Default)]
pub struct FallingObject;

impl Paddle {
    fn new(side: Side, width: f32, height: f32) -> Paddle {
        Paddle {
            side,
            width: width,
            height: height,
        }
    }
}

impl Component for FallingObject {
    type Storage = NullStorage<Self>;
}

impl Component for Paddle {
    type Storage = DenseVecStorage<Self>;
}

fn load_sprite_spreadsheet(world: &mut World) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/pong_spritesheet.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "texture/pong_spritesheet.ron",
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

fn read_arena_width_and_height(world: &World) -> (f32, f32) {
    let arena_config = &world.read_resource::<PongConfig>().arena;
    (arena_config.width, arena_config.height)
}

fn read_paddle_width_and_height(world: &World) -> (f32, f32) {
    let paddle_config = &world.read_resource::<PongConfig>().paddle;
    (paddle_config.width, paddle_config.height)
}

fn read_ball_radius_and_velocity(world: &World) -> (f32, Vector2<f32>) {
    let ball_config = &world.read_resource::<PongConfig>().ball;
    (ball_config.radius, ball_config.velocity)
}

fn initialise_camera(world: &mut World) {
    let mut transform = Transform::default();
    let (arena_width, arena_height) = read_arena_width_and_height(world);

    transform.set_translation_xyz(arena_width * 0.5, arena_height * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(arena_width, arena_height))
        .with(transform)
        .build();
}

fn create_paddle(world: &mut World, side: Side, transform: Transform, sprite_render: SpriteRender) {
    let (paddle_width, paddle_height) = read_paddle_width_and_height(world);
    world
        .create_entity()
        .with(sprite_render)
        .with(Paddle::new(side, paddle_width, paddle_height))
        .with(transform)
        .build();
}

fn initialise_paddles(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    let mut left_transform = Transform::default();
    let mut right_transform = Transform::default();

    let (arena_width, arena_height) = read_arena_width_and_height(world);
    let (paddle_width, _) = read_paddle_width_and_height(world);
    let y = arena_height / 2.0;
    left_transform.set_translation_xyz(paddle_width * 0.5, y, 0.0);
    right_transform.set_translation_xyz(arena_width - paddle_width * 0.5, y, 0.0);

    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet.clone(),
        sprite_number: 0,
    };

    create_paddle(world, Side::Left, left_transform, sprite_render.clone());
    create_paddle(world, Side::Right, right_transform, sprite_render.clone());
}

fn initialise_ball(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    let (ball_radius, ball_velocity) = read_ball_radius_and_velocity(world);
    let mut local_transform = Transform::default();
    let (arena_width, arena_height) = read_arena_width_and_height(world);
    local_transform.set_translation_xyz(arena_width / 2.0, arena_height / 2.0, 0.0);

    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet_handle,
        sprite_number: 1,
    };

    world
        .create_entity()
        .with(sprite_render)
        .with(Ball {
            radius: ball_radius,
            velocity: [ball_velocity.x, ball_velocity.y],
        })
        .with(local_transform)
        .build();
}

fn initialise_scoreboard(world: &mut World) {
    let font = world.read_resource::<Loader>().load(
        "font/square.ttf",
        TtfFormat,
        (),
        &world.read_resource(),
    );

    let p1_transform = UiTransform::new(
        "P1".to_string(),
        Anchor::TopMiddle,
        Anchor::TopMiddle,
        -50.,
        -50.,
        1.,
        200.,
        50.,
    );
    let p2_transform = UiTransform::new(
        "P2".to_string(),
        Anchor::TopMiddle,
        Anchor::TopMiddle,
        50.,
        -50.,
        1.,
        200.,
        50.,
    );

    let p1_score = world
        .create_entity()
        .with(p1_transform)
        .with(UiText::new(
            font.clone(),
            "0".to_string(),
            [1., 1., 1., 1.],
            50.,
        ))
        .build();

    let p2_score = world
        .create_entity()
        .with(p2_transform)
        .with(UiText::new(
            font.clone(),
            "0".to_string(),
            [1., 1., 1., 1.],
            50.,
        ))
        .build();

    world.insert(ScoreText { p1_score, p2_score });
}
