# pong

## How to run

**IMPORTANT NOTE:** this is based on the project explained in the [Amethyst Book](https://book.amethyst.rs/master/pong-tutorial.html) "Pong Tutorial". This might drift in time from the book.

To run the game, use

```
cargo run --features "vulkan"
```

on Windows and Linux, and

```
cargo run --features "metal"
```

on macOS.

For building without any graphics backend, you can use

```
cargo run --features "empty"
```

but be aware that as soon as you need any rendering you won't be able to run your game when using
the `empty` feature.
